FROM openjdk:8
ADD target/*.war /usr/app/
RUN mkdir -p /usr/app/resources/
WORKDIR /usr/app
ADD startup.sh /usr/app/startup.sh
#CMD ["sh", "/usr/app/startup.sh"]
